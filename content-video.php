<?php
/**
 * The template for displaying posts in the Video post format
 *
 * @package WordPress
 * @subpackage oivl
 * @since oivl
 */
?>

<?php
$url = get_the_content();
preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $id);
if(!empty($id)) {
   $id = $id[0];
}
?>
     <div class="row video">

       <div class="col-sm-6">
        <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>" class="embed-responsive embed-responsive-16by9">
            <div style="background-image:url(http://i.ytimg.com/vi/<?php idvideo(); ?>/hqdefault.jpg)" class="embed-responsive-item"></div><span><?php timevideo($id);?></span></a>
       </div>

        <div class="col-sm-6">
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <ul>
            <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' trước'; ?></li>
            <li><?php echo do_shortcode("[post_view]"); ?> lượt xem</li>
<!--            <li> <fb:comments-count href="<?php //the_permalink(); ?>"></fb:comments-count></li>-->
        </ul>
        </div>

    </div>
