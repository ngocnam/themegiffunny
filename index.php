<?php
/*
 * Template khohai cho wordpress
 * Author: Nguyễn Ngọc Nam
 * Facebook author:
 */
get_header(); ?>

    <main role="main">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                   <!--begin -->
                   <?php
                     if ( have_posts() ) :
                       // Start the Loop.
                       while ( have_posts() ) : the_post();

                         /*
                          * Include the post format-specific template for the content. If you want to
                          * use this in a child theme, then include a file called called content-___.php
                          * (where ___ is the post format) and that will be used instead.
                          */
                      
                         get_template_part( 'content', get_post_format() );

                       endwhile;
                       // Previous/next post navigation.
                       xemthem( 'nav-below' );

                     else :
                       // If no content, include the "No posts found" template.
                       get_template_part( 'content', 'none' );

                     endif;
                   ?>
                    <!-- end -->
                </div>
                <aside class="col-md-4 left-home">
                    <div class="sidebar-panel hidden-xs">
                        <div class="clearfix"> <h3>Nổi bật gần đây</h3></div>
						
						 <?php
							
							query_posts(array('orderby' => 'rand', 'showposts' => 10));
							$count = 0; 
							if (have_posts()) :while (have_posts()) : the_post();  
								if($count % 3==0):
										if($count != 0): ?> </div> <?php endif;?>
										<div class="row">
										<?php endif; $count++;?>
									
							<div class="col-xs-12 col-sm-4 col-md-12">
                                <div class="row sub-video">
                                    <div class="col-xs-6 col-sm-12 col-md-6">
                                        <a title="<?php the_title();?>" href="<?php the_permalink(); ?>" class="embed-responsive embed-responsive-16by9">
                                           <?php the_post_thumbnail( 'thumbnail', array( 'class' => 'embed-responsive-item' ) ); ?>
										</a>
                                    </div>
                                    <div class="col-xs-6 col-sm-12 col-md-6">
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>
                                        <span class="hidden-sm">
                                            <?php echo do_shortcode("[post_view]"); ?> lượt xem
                                        </span>
                                        <span class="hidden-sm">
                                            <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' trước'; ?>
                                        </span>
                                        </div>
                                </div>
                            </div>							
										
								   
							<?php 
								endwhile;
								if($count % 3 != 0 ): ?></div> <?php  endif;
 								endif;											
						 ?>	
							
                    </div>
                </aside>
            </div>
        </div>
    </main>
<!--footer-->
<?php get_footer(); ?>

</body>

</html>
