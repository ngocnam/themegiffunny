<?php /* * Template haivl.tv cho wordpress * Author: Ngọc Nam * Facebook author: */ ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
    <meta content='khohai.com - clip hot' name='author' />
    <link href='https://plus.google.com/109149508993619552608' rel='publisher' />
    <link href='https://plus.google.com/109149508993619552608' rel='author' />
    <meta content="559815264149051" property='fb:app_id' />
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width" />
    <title>
       
        <?php bloginfo( 'name' ); ?>
    </title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
    <!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
    <?php wp_head(); ?>
    <link rel="stylesheet" href="<?php echo bloginfo('template_directory') . '/css/bootstrap.min.css';?>" />
    <link rel="stylesheet" href="<?php echo bloginfo('template_directory') . '/css/bootstrap-theme.min.css';?>" />
     <link rel="stylesheet" href="<?php echo bloginfo('template_directory') . '/style.css';?>" />


    <script src="<?php echo bloginfo('template_directory') . '/js/jquery-1.11.1.min.js';?>"></script>
    <script src="<?php echo bloginfo('template_directory') . '/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo bloginfo('template_directory') . '/js/script.js';?>"></script>
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', '']); //UA-37498333-1
        _gaq.push(['_setDomainName', '']); //clipcuoi.vn
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
    </script>

</head>
<body>

    <nav class="navbar navbar-default navbar-fixed-top navbar-nham" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo-kho-hai" href="<?php echo get_home_url(); ?>">
                  <img src="<?php echo bloginfo('template_directory') . '/images/khohai.png'?>" alt="">
                </a>

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?php echo get_home_url(); ?>" class="new-link">
                            Mới nhất
                        </a>

                    </li>
                    <li>
                        <a href="#" class="hot-link">
                            Hot nhất
                        </a>
                    </li>
                </ul>
                <!--
                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Nhập từ khoá tìm kiếm">
                    </div>
                    <button type="submit" class="btn btn-default">Tìm kiếm</button>
                </form>
-->
                <ul class="nav navbar-nav navbar-right social">
                    <li>
                        <a href="#">
                            <img src="<?php echo bloginfo('template_directory') . '/images/facebook-icon.png'?>" alt="facebook">
                        </a>
                        <a href="#">
                            <img src="<?php echo bloginfo('template_directory') . '/images/twitter-icon.png'?>" alt="twitter">
                        </a>
                        <a href="#">
                            <img src="<?php echo bloginfo('template_directory') . '/images/google-icon.png'?>" alt="google plus">
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
