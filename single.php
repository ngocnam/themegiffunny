<?php
/*
 * Template haivl.tv cho wordpress
 * Author: Namnn
 * Facebook 
 */
get_header(); ?>

    <main role="main">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                   <!--begin -->
                   <?php
                     if ( have_posts() ) :
                       // Start the Loop.
                       while ( have_posts() ) : the_post();
							 ?>
							    <div class="navigation-gif row">
								
									<div class="col-sm-6 overme"><?php previous_post_link(); ?> </div>
									<div class="col-sm-6 overmer" ><?php next_post_link(); ?></div>
								
								</div>
								
								<div class="embed-responsive embed-responsive-16by9 media-player">
									<?php the_content();?>
								</div>
                                <div> <h3><?php the_title();?></h3></div>
                                <div class="row">
                                    <div class="col-sm-6">
                                         <div class="fb-like" data-href="<?php the_permalink() ?>" data-send="false" data-layout="button_count" data-width="90" data-show-faces="false" data-share="true">
                                         </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="text-right"> <?php echo do_shortcode("[post_view]"); ?>  lượt xem</div>                                    
                                    </div>
                                </div>
                                
                
							 <?php
                         
                       endwhile;
                       // Previous/next post navigation.


                     else :
                       // If no content, include the "No posts found" template.
                       get_template_part( 'content', 'none' );

                     endif;
                   ?>
                    <!-- end -->
                     <aside class="cc-section">
                        <div class="clearfix">
                            <h4>Có thể bạn sẽ thích</h4></div>
                      <?php 
                            global $timings, $time_shortcuts, $bawpvc_options;
                            extract(shortcode_atts(array(
                                'number' => 9,
                                'show' => 1,
                                'time' => 'all',
                                'date' => '',
                                'before' => '',
                                'after' => '',
                                'ul_class' => 'pvc',
                                'li_class' => 'pvc',
                                'order' => 'DESC',
                                'author' => '',
                                'post_type' => $bawpvc_options['post_types']
                            ), $atts));

                            $date = $date != '' ? $date : date( $timings[$time] );
                            $date = $time == 'all' ? '' : '-' . $date;
                            $ul_class = $ul_class!='' ? ' class="' . sanitize_html_class( $ul_class ) . '"' : '';
                            $li_class = $ul_class!='' ? ' class="' . sanitize_html_class( $li_class ) . '"' : '';
                            $order = $order == 'ASC' ? 'ASC' : 'DESC';
                            $author_name = '';
                            if( !is_numeric( $author ) ):
                                $author_name = $author;
                                $author = '';
                            endif;
                            $meta_key = apply_filters( 'baw_count_views_meta_key', '_count-views_' . $time . $date, $time, $date );
                            $post_type = is_array( $post_type ) ? $post_type : explode( ',', $post_type );
                            ob_start();
                            $current = $post->ID;

                            $exclude_ids = array($current);
                            $r = new WP_Query( array(	'posts_per_page' => $number, 
                                                        'no_found_rows' => true, 
                                                        'post_status' => 'publish', 
                                                        'post_type' => $post_type,
                                                        'ignore_sticky_posts' => true, 
                                                        'meta_key' => $meta_key, 
                                                        'meta_value_num' => '0', 
                                                        'post__not_in' => $exclude_ids,
                                                        'meta_compare' => '>', 
                                                        'orderby' => 'meta_value_num',
                                                        'author' => $author,
                                                        'author_name' => $author_name,
                                                        'order' => $order )
                                                    );
                            if ($r->have_posts()) :
                                echo $before; $countv=0; ?>
                                <?php  while ($r->have_posts()) : $r->the_post(); ?>
                                <?php 	
                                    $count = '';
                                    if( $show ):
                                        $count = (int)get_post_meta( get_the_ID(), $meta_key, true );
                                        do_action( 'baw_count_views_count_action', $count, $meta_key, $time, $date, get_the_ID() );
                                        $count = apply_filters( 'baw_count_views_count', $count, $meta_key, $time, $date );
                                    endif;
                                    //namnn edit 
                                    if($countv % 3==0):
                                    if($countv != 0): ?> </div> <?php endif;?>
                                        <div class="row ">
                                    <?php endif; $countv++;
                                    //namnn edit 

                                ?>

                                   <div class="col-sm-4">
                                                        <div class="row svi">
                                                            <div class="col-xs-6 col-sm-12">
                                                                <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>" class="embed-responsive embed-responsive-16by9">
                                                                     <?php the_post_thumbnail( 'thumbnail', array( 'class' => 'embed-responsive-item' ) ); ?>
                                                                </a>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-12">
                                                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3><span><?php echo do_shortcode("[post_view]"); ?> lượt xem</span></div>
                                                        </div>
                                 </div>  
                                <?php endwhile; if($countv % 3 != 0 ): ?></div> <?php  endif;?>

                                <?php echo $after;
                                // Reset the global $the_post as this query will have stomped on it
                                wp_reset_postdata();
                            endif;

                      ?>
                       
                    </aside>
                    
                </div>
                <aside class="col-md-4 left-home">
                    <div class="sidebar-panel hidden-xs">
                        <div class="clearfix"> <h3>Gợi ý cho bạn</h3></div>
						
						<?php //echo do_shortcode("[most_view]"); ?>
						 <?php
							
							query_posts(array('orderby' => 'rand', 'showposts' => 8));
							$count = 0; 
							if (have_posts()) :while (have_posts()) : the_post();  
								if($count % 3==0):
										if($count != 0): ?> </div> <?php endif;?>
										<div class="row">
										<?php endif; $count++;?>
									
							<div class="col-xs-12 col-sm-4 col-md-12">
                                <div class="row sub-video">
                                    <div class="col-xs-6 col-sm-12 col-md-6">
                                        <a title="Cú nhảy cầu thốn nhất năm" href="<?php the_permalink(); ?>" class="embed-responsive embed-responsive-16by9">
                                           <?php the_post_thumbnail( 'thumbnail', array( 'class' => 'embed-responsive-item' ) ); ?>
										</a>
                                    </div>
                                    <div class="col-xs-6 col-sm-12 col-md-6">
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3><span class="hidden-sm">
                                            <?php echo do_shortcode("[post_view]"); ?> lượt xem
                                        </span>
                                        <span class="hidden-sm">
                                            <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' trước'; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>							
										
								   
							<?php 
								endwhile;
								if($count % 3 != 0 ): ?></div> <?php  endif;
 								endif;											
						 ?>	
							
                    </div>
                </aside>
            </div>
        </div>
    </main>


<!--footer-->
<?php get_footer(); ?>
