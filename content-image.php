<?php
/**
 * The template for displaying posts in the Image post format
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<?php
$url = get_the_content();
preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $id);
if(!empty($id)) {
   $id = $id[0];
}
?>
     <div class="row video">

       <div class="col-sm-6">
        	<?php the_content();?>
       </div>

        <div class="col-sm-6">
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <ul>
            <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' trước'; ?></li>
            <li><?php echo do_shortcode("[post_view]"); ?> lượt xem</li>
<!--            <li> <fb:comments-count href="<?php //the_permalink(); ?>"></fb:comments-count></li>-->
        </ul>
        </div>

    </div>
